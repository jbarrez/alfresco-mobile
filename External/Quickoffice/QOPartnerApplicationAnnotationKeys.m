#import "QOPartnerApplicationAnnotationKeys.h"

NSString* const QuickofficeApplicationSecretUUIDKey = @"PartnerApplicationSecretUUID";
NSString* const QuickofficeApplicationInfoKey = @"PartnerApplicationInfo";
NSString* const QuickofficeApplicationIdentifierKey = @"PartnerApplicationIdentifier";

NSString* const QuickofficeApplicationDocumentExtension = @"alf01";
NSString* const QuickofficeApplicationDocumentExtensionKey = @"PartnerApplicationDocumentExtension";

NSString* const QuickofficeApplicationDocumentUTI = @"com.alfresco.mobile.qpa";
NSString* const QuickofficeApplicationDocumentUTIKey = @"PartnerApplicationDocumentUTI";

/* Alfresco additional */
NSString * const QuickofficeBundleIdentifier = @"com.quickoffice.quickofficeipad";
